import struct
import socket
from collections import OrderedDict


##
# Utility functions
##
def get_mask(addr):
    if not addr:
        return [None, None]
    ret = addr.split('/')
    if len(ret) == 2:
        return ret
    else:
        return [addr, None]


##
# Expressions generators
##
def genex(name, kwarg):
    exp_data = []
    for key, value in kwarg.items():
        exp_data.append(('NFTA_%s_%s' % (name.upper(), key.upper()), value))
    return {'attrs': [('NFTA_EXPR_NAME', name),
                      ('NFTA_EXPR_DATA', {'attrs': exp_data})]}


def verdict(code):
    kwarg = OrderedDict()
    kwarg['dreg'] = 0  # NFT_REG_VERDICT
    kwarg['data'] = {'attrs': [('NFTA_DATA_VERDICT',
                                {'attrs': [('NFTA_VERDICT_CODE', code)]})]}
    return [genex('immediate', kwarg), ]


def counter(bytes=0,packets=0):
    kwarg = OrderedDict()
    kwarg['bytes'] = bytes
    kwarg['packets'] = packets
    return [genex('counter', kwarg), ]


def notrack():
    return [{'attrs': [('NFTA_EXPR_NAME','notrack')]}]


def mark(match=None,cmp_op=0,new=None):
    ret = []
    if match is not None:
        kwarg = OrderedDict()
        kwarg['key'] = 3  # NFT_META_MARK
        kwarg['dreg'] = 1
        ret.append(genex('meta', kwarg))
        kwarg = OrderedDict()
        kwarg['sreg'] = 1
        kwarg['op'] = cmp_op
        kwarg['data'] = {'attrs': [('NFTA_DATA_VALUE',struct.pack('I',match))]}
        ret.append(genex('cmp', kwarg))
    if new is not None:
        kwarg = OrderedDict()
        kwarg['dreg'] = 1
        kwarg['data'] = {'attrs': [('NFTA_DATA_VALUE', struct.pack('I',new))]}
        ret.append(genex('immediate', kwarg))
        kwarg = OrderedDict()
        kwarg['key'] = 3  # NFT_META_MARK
        kwarg['sreg'] = 1
        ret.append(genex('meta', kwarg))
    return ret


def l4proto(proto,cmp_op=0):
    ret = []
    kwarg = OrderedDict()
    kwarg['key'] = 16  # NFT_META_L4PROTO
    kwarg['dreg'] = 1
    ret.append(genex('meta', kwarg))
    kwarg = OrderedDict()
    kwarg['sreg'] = 1
    kwarg['op'] = cmp_op
    kwarg['data'] = {'attrs': [('NFTA_DATA_VALUE',proto)]}
    ret.append(genex('cmp', kwarg))
    return ret


def dscp(match=None,cmp_op=0,new=None,family=socket.AF_INET):
    ret = []
    if family == socket.AF_INET6:
        offset = 0
        length = 2
        shift = 6
        packchr = 'H'
    else:
        offset = 1
        length = 1
        shift = 0
        packchar = 'B'
    kwarg = OrderedDict()
    kwarg['dreg'] = 1
    kwarg['base'] = 1  # NFT_PAYLOAD_NETWORK_HEADER
    kwarg['offset'] = offset
    kwarg['len'] = length
    ret.append(genex('payload', kwarg))
    if match is not None:
        kwarg = OrderedDict()
        kwarg['sreg'] = 1
        kwarg['dreg'] = 2 if new is not None else 1
        kwarg['len'] = length
        kwarg['mask'] = {'attrs': [('NFTA_DATA_VALUE', struct.pack('!%s' % (packchr,),(2**6-1) << shift))]}
        kwarg['xor'] = {'attrs': [('NFTA_DATA_VALUE', struct.pack('!%s' % (packchr,),0))]}
        ret.append(genex('bitwise', kwarg))
        kwarg = OrderedDict()
        kwarg['sreg'] = 2 if new is not None else 1
        kwarg['op'] = cmp_op
        kwarg['data'] = {'attrs': [('NFTA_DATA_VALUE', struct.pack('!%s' % (packchr,),match << shift))]}
        ret.append(genex('cmp', kwarg))
    if new is not None:
        kwarg = OrderedDict()
        kwarg['sreg'] = 1
        kwarg['dreg'] = 1
        kwarg['len'] = length
        if family == socket.AF_INET6:
            kwarg['mask'] = {'attrs': [('NFTA_DATA_VALUE', struct.pack('!H',61503))]}
            kwarg['xor'] = {'attrs': [('NFTA_DATA_VALUE', struct.pack('!H',new << 6))]}
        else:
            kwarg['mask'] = {'attrs': [('NFTA_DATA_VALUE', struct.pack('B',2**6-1))]}
            kwarg['xor'] = {'attrs': [('NFTA_DATA_VALUE', struct.pack('B',new << 6))]}
        ret.append(genex('bitwise', kwarg))
        kwarg = OrderedDict()
        kwarg['sreg'] = 1
        kwarg['base'] = 1  # NFT_PAYLOAD_NETWORK_HEADER
        kwarg['offset'] = offset
        kwarg['len'] = length
        kwarg['csum_type'] = 0  # NFT_PAYLOAD_CSUM_NONE
        kwarg['csum_offset'] = 0
        kwarg['csum_flags'] = 0
        ret.append(genex('payload', kwarg))
    return ret


def ipv4addr(src=None, dst=None):
    if not src and not dst:
        raise ValueError('must be at least one of src, dst')

    ret = []
    # get masks
    src, src_mask = get_mask(src)
    dst, dst_mask = get_mask(dst)

    # load address(es) into NFT_REG_1
    kwarg = OrderedDict()
    kwarg['dreg'] = 1  # save to NFT_REG_1
    kwarg['base'] = 1  # NFT_PAYLOAD_NETWORK_HEADER
    kwarg['offset'] = 12 if src else 16
    kwarg['len'] = 8 if (src and dst) else 4
    ret.append(genex('payload', kwarg))

    # run bitwise with masks -- if provided
    if src_mask or dst_mask:
        mask = b''
        if src:
            if not src_mask:
                src_mask = '32'
            src_mask = int('1' * int(src_mask), 2)
            mask += struct.pack('I', src_mask)
        if dst:
            if not dst_mask:
                dst_mask = '32'
            dst_mask = int('1' * int(dst_mask), 2)
            mask += struct.pack('I', dst_mask)
        xor = '\x00' * len(mask)
        kwarg = OrderedDict()
        kwarg['sreg'] = 1  # read from NFT_REG_1
        kwarg['dreg'] = 1  # save to NFT_REG_1
        kwarg['len'] = 8 if (src and dst) else 4
        kwarg['mask'] = {'attrs': [('NFTA_DATA_VALUE', mask)]}
        kwarg['xor'] = {'attrs': [('NFTA_DATA_VALUE', xor)]}
        ret.append(genex('bitwise', kwarg))

    # run cmp
    packed = b''
    if src:
        packed += socket.inet_aton(src)
    if dst:
        packed += socket.inet_aton(dst)
    kwarg = OrderedDict()
    kwarg['sreg'] = 1  # read from NFT_REG_1
    kwarg['op'] = 0  # NFT_CMP_EQ
    kwarg['data'] = {'attrs': [('NFTA_DATA_VALUE', packed)]}
    ret.append(genex('cmp', kwarg))
    return ret


def ipv6addr(src=None, dst=None):
    if not src and not dst:
        raise ValueError('must be at least one of src, dst')

    ret = []
    # get masks
    src, src_mask = get_mask(src)
    dst, dst_mask = get_mask(dst)

    # load address(es) into NFT_REG_1
    kwarg = OrderedDict()
    kwarg['dreg'] = 1  # save to NFT_REG_1
    kwarg['base'] = 1  # NFT_PAYLOAD_NETWORK_HEADER
    kwarg['offset'] = 8 if src else 24
    kwarg['len'] = 32 if (src and dst) else 16
    ret.append(genex('payload', kwarg))

    # run bitwise with masks -- if provided
    if src_mask or dst_mask:
        mask = b''
        if src:
            if not src_mask:
                src_mask = '128'
            src_mask = int('1' * int(src_mask), 2)
            mask += struct.pack('QQ', src_mask & 0xffffffffffffffff, src_mask >> 64)
        if dst:
            if not dst_mask:
                dst_mask = '128'
            dst_mask = int('1' * int(dst_mask), 2)
            mask += struct.pack('QQ', dst_mask & 0xffffffffffffffff, dst_mask >> 64)
        xor = '\x00' * len(mask)
        kwarg = OrderedDict()
        kwarg['sreg'] = 1  # read from NFT_REG_1
        kwarg['dreg'] = 1  # save to NFT_REG_1
        kwarg['len'] = 32 if (src and dst) else 16
        kwarg['mask'] = {'attrs': [('NFTA_DATA_VALUE', mask)]}
        kwarg['xor'] = {'attrs': [('NFTA_DATA_VALUE', xor)]}
        ret.append(genex('bitwise', kwarg))

    # run cmp
    packed = b''
    if src:
        packed += socket.inet_pton(socket.AF_INET6,src)
    if dst:
        packed += socket.inet_pton(socket.AF_INET6,dst)
    kwarg = OrderedDict()
    kwarg['sreg'] = 1  # read from NFT_REG_1
    kwarg['op'] = 0  # NFT_CMP_EQ
    kwarg['data'] = {'attrs': [('NFTA_DATA_VALUE', packed)]}
    ret.append(genex('cmp', kwarg))
    return ret


def ipvXaddr(family=socket.AF_INET, src=None, dst=None):
    if family == socket.AF_INET:
        return ipv4addr(src=src, dst=dst)
    elif family == socket.AF_INET6:
        return ipv6addr(src=src, dst=dst)
    else:
        raise ValueError("family must be AF_INET or AF_INET6")
